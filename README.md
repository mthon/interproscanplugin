Authors
=======
Michael Thon mike@michaelrthon.com

Description
===========
The Geneious InterProScan Plugin enables you to run InterProScan from within the
Geneious application.

Installation
============
For Geneious users, you can install the plugin from the plugin manager. Start
Geneious and go to the menu Tools > Plugins.

Development Notes
=================

I'm currently developing the plugin in NetBeans and I don't know if the included
ant file will work in other environments.  Eventually I'd like it to work on the
command line and/or in Eclipse, but its not a priority for me.

The project depends on the following jars, which are not included in the repo.
You should download them yourself and put them in a subdirectory called 'lib'.

RunIprScan.jar             saaj.jar
axis.jar                   servlet.jar
commons-discovery-0.2.jar  wsdl4j-1.5.1.jar
commons-logging-1.0.4.jar  xercesImpl.jar
jaxrpc.jar                 xmlParserAPIs.jar
jgrapht-jdk1.5.jar
