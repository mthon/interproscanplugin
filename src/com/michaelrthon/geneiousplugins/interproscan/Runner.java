/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michaelrthon.geneiousplugins.interproscan;

import com.biomatters.geneious.publicapi.documents.AnnotatedPluginDocument;
import com.biomatters.geneious.publicapi.documents.sequence.AminoAcidSequenceDocument;
import com.biomatters.geneious.publicapi.documents.sequence.SequenceDocument;
import com.biomatters.geneious.publicapi.plugin.DocumentOperationException;
import com.biomatters.geneious.publicapi.plugin.Options;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator.AnnotationGeneratorResult;
import com.biomatters.geneious.publicapi.utilities.FileUtilities;
import com.michaelrthon.runiprscan.IprOperation;
import com.michaelrthon.runiprscan.IprQueue;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jebl.util.CompositeProgressListener;
import jebl.util.ProgressListener;

/**
 *
 * @author Michael Thon <mike@michaelrthon.com>
 */
public class Runner implements IprQueue.IprQueueDelegate {

    private CompositeProgressListener progress = null;
    private IprQueue queue = null;
    private ArrayList<IprOperation> opsList = null;
    private static List<Runner> runners = Collections.synchronizedList(new ArrayList<Runner>());

    public List<SequenceAnnotationGenerator.AnnotationGeneratorResult> scanSeqs(
            AnnotatedPluginDocument[] documents,
            SequenceAnnotationGenerator.SelectionRange selectionRange,
            ProgressListener progressListener,
            Options options)
            throws DocumentOperationException {

        List<SequenceAnnotationGenerator.AnnotationGeneratorResult> resultsList =
                new ArrayList<SequenceAnnotationGenerator.AnnotationGeneratorResult>();

        queue = new IprQueue();
        queue.setRunQueueSize(30);
        //PluginQueue queue = new PluginQueue();
        queue.delegate = this;
        InterproscanOptions ops = (InterproscanOptions) options;
        String iprApps[] = getInterProApps(ops);

        String email = ops.getEmailAddress();

        String featType = ops.getFeatureTypeOption();
        boolean extraFeats = ops.getExtraFeature();

        int batchCount = (documents.length / 10) * 3;
        if ((documents.length % 10) > 0) {
            batchCount+= 3;
        }
        batchCount += 1;
        int jobCount = batchCount + (documents.length);
//        if (Runner.scanGens == null) {
//            Runner.scanGens = Collections.synchronizedList(new ArrayList<InterproscanAnnotationGenerator>());
//        }
//        InterproscanAnnotationGenerator.scanGens.add(this);

        progress = new CompositeProgressListener(progressListener, jobCount);
        Runner.runners.add(this);
        //progress.beginSubtask("Waiting for other InterProScan analyses to finish...");
//        while (InterproscanAnnotationGenerator.scanGens.indexOf(this) != 0) {
//            try {
//                Thread.sleep((long) 0.5);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(InterproscanAnnotationGenerator.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
        //progress.setIndeterminateProgress();
        //progress.beginSubtask("Sending Sequences To InterProScan");
        // the results need to be returned in the same order as the sequence documents
        // Since RunIprScan returns them in the order they are recieved from the web
        // service, we need to keep a list of operations for later processing.
        opsList = new ArrayList<IprOperation>();
        for (AnnotatedPluginDocument annotatedPluginDocument : documents) {

            SequenceDocument seqDoc = (AminoAcidSequenceDocument) annotatedPluginDocument.getDocument();

            String trimSeq = trimTerminator(seqDoc.getSequenceString());

            IprOperation op = new IprOperation(seqDoc.getName(), trimSeq, email);
            op.setAppList(iprApps);

            File tempFile = null;
            try {
                //tempFile = File.createTempFile(seqDoc.getName(), "");
                tempFile = FileUtilities.createTempFile(seqDoc.getName(), "", true);
            } catch (IOException ex) {
                Logger.getLogger(InterproscanAnnotationGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
            op.outputFile = tempFile;
            queue.addOperation(op);
            opsList.add(op);
        }

        queue.runQueue();

        for (IprOperation op : opsList) {
            //String xml = op.getResultXml();

            XmlParser p = new XmlParser(op.getResultXml());
            p.setSeqLength(op.getSeqLength());
            p.setFeatType(featType);
            p.setMakeExtraFeats(extraFeats);

            AnnotationGeneratorResult r = p.parseXml();
            resultsList.add(r);
        }
        progress.setComplete();
        return resultsList;
    }

    private String trimTerminator(String s) {
        if (s.endsWith("*")) {
            //System.out.println("found one with a stop");
            return s.substring(0, (s.length() - 1));
        } else {
            return s;
        }
    }

    private String[] getInterProApps(InterproscanOptions ops) {
        ArrayList<String> apps = new ArrayList<String>();
        //String apps[] = null;
        //StringBuilder appb = new StringBuilder();
        if (ops.getProdom().getValue() ) {
            apps.add("ProDom");
        }
        if (ops.getPrints().getValue()) {
            apps.add("PRINTS");
        }
        if (ops.getPirsf().getValue()) {
            apps.add("PIRSF");
        }
        if (ops.getPfama().getValue()) {
            apps.add("PfamA");
        }
        if (ops.getSmart().getValue()) {
            apps.add("SMART");
        }
        if (ops.getTigrfam().getValue()) {
            apps.add("TIGRFAM");
        }
        if (ops.getPrositeProfiles().getValue()) {
            apps.add("PrositeProfiles");
        }
        if (ops.getHamap().getValue()) {
            apps.add("HAMAP");
        }
        if (ops.getPrositePatterns().getValue()) {
            apps.add("PrositePatterns");
        }
        if (ops.getSuperFamily().getValue()) {
            apps.add("SuperFamily");
        }
        if (ops.getSignalp().getValue()) {
            apps.add("SignalP");
        }
        if (ops.getTmhmm().getValue()) {
            apps.add("TMHMM");
        }
        if (ops.getPanther().getValue()) {
            apps.add("Panther");
        }
        if (ops.getGene3d().getValue()) {
            apps.add("Gene3d");
        }
        if(ops.getPhobius().getValue()) {
            apps.add("Phobius");
        }
        
        String[] retArray = new String[1];
        return apps.toArray(retArray);
    }

    public void operationFinished(IprOperation op) {
        //System.out.println("an op is finished - beginning subtask");
        progress.beginSubtask("Gathering InterPro results");
    }


    public void queueFinished() {
        Runner.runners.remove(this);
    }

 
    public void queueLoop() {

        if (progress.isCanceled()) {
            queue.setIsCancelled(true);
            opsList = new ArrayList<IprOperation>();
            Runner.runners.remove(this);
            //queue = null;
        }
    }

    public void submittingBatch() {
        String taskText = null;
        //System.out.println("waiting for other jobs");
        progress.beginNextSubtask("Waiting for other InterProScan jobs");
        
        while (Runner.runners.indexOf(this) > 0) {
            //System.out.println("sleeping the thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Runner.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (progress.isCanceled()) {
                //System.out.println("job is cancelled");
                queue.setIsCancelled(true);
                opsList = new ArrayList<IprOperation>();
                Runner.runners.remove(this);
                //queue = null;
            }
        }

        if (queue.queue.size() > 1) {
            taskText = "Submitting a batch of sequences to the InterProScan server";
        } else {
            taskText = "Submitting a sequence to the InterProScan server";
        }
        //System.out.println("beginning a subtask");
        progress.beginNextSubtask(taskText);

    }

    public void collectingResults() {
        //System.out.println("collecting results");
        progress.beginNextSubtask("Running InterProScan");

    }
}
