package com.michaelrthon.geneiousplugins.interproscan;

import com.biomatters.geneious.publicapi.plugin.GeneiousPlugin;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator;

/**
 * Generic command line plugin template
 */
public class InterproscanPlugin extends GeneiousPlugin {

    @Override
    public String getAuthors() {
        return "Michael Thon";
    }

    @Override
    public String getDescription() {
        return "Search InterPro, an integrated database of protein signatures";
    }

    @Override
    public String getHelp() {
        return InterproscanAnnotationGenerator.HELP;
    }

    @Override
    public int getMaximumApiVersion() {
        return 4;
    }

    @Override
    public String getMinimumApiVersion() {
        return "4.14";
    }

    @Override
    public String getName() {
        return "InterProScan";
    }

    @Override
    public String getVersion() {
        return "1.1.6";
    }

    @Override
    public SequenceAnnotationGenerator[] getSequenceAnnotationGenerators() {
        return new SequenceAnnotationGenerator[]{
                    new InterproscanAnnotationGenerator()
                };
    }
    
    @Override
    public String getEmailAddressForCrashes() {
      return "mike@michaelrthon.com";
    }
}
