package com.michaelrthon.geneiousplugins.interproscan;

import com.biomatters.geneious.publicapi.components.GEditorPane;
import com.biomatters.geneious.publicapi.plugin.Options;
import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.List;
import javax.swing.JPanel;
import org.virion.jam.html.SimpleLinkListener;

class InterproscanOptions extends Options {

    private StringOption emailAddress;
    private ComboBoxOption<OptionValue> featureType;
    private BooleanOption extraFeature;
    
    //the currently supported member databases
    //{"ProDom", "PRINTS", "PIRSF", "PfamA", "SMART",
    //    "TIGRFAM", "PrositeProfiles", "HAMAP", "PrositePatterns", "SuperFamily",
    //    "SignalP", "TMHMM", "Panther", "Gene3d", "Phobius"};
    private BooleanOption prodom;
    private BooleanOption prints;
    private BooleanOption pirsf;
    private BooleanOption pfama;
    private BooleanOption smart;
    private BooleanOption tigrfam;
    private BooleanOption prositeProfiles;
    private BooleanOption hamap;
    private BooleanOption prositePatterns;
    private BooleanOption superFamily;
    private BooleanOption signalp;
    private BooleanOption tmhmm;
    private BooleanOption panther;
    private BooleanOption gene3d;
    private BooleanOption phobius;

    
    
    public final static String FEAT_SEPARATE = "separately";
    public final static String FEAT_QUALIFIERS = "qualifiers";

    public InterproscanOptions() {
        emailAddressOptions();
        extraFeatureOptions();
        featureTypeOptions();
        appTypes();

    }

    private void emailAddressOptions() {

        emailAddress = addStringOption("emailAddress", "Email Address", "");
        emailAddress.setDescription("For reporting InterProScan errors");
    }

    private void extraFeatureOptions() {
        extraFeature = addBooleanOption("emptyFeature", "Add features to proteins without InterPro results or with errors", true);
        extraFeature.setDescription("Adds special features to proteins without matches to the InterPro Database or that have errors");
    }

    private void featureTypeOptions() {

        OptionValue val1 = new OptionValue(FEAT_SEPARATE, "Separately",
                "Show InterPro terms as separate features that span the length of the protein");

        OptionValue val2 = new OptionValue(FEAT_QUALIFIERS, "As Qualifiers",
                "Show InterPro terms as qualifiers on the predicted domains");
        List<OptionValue> values = Arrays.asList(val1, val2);

        featureType = addComboBoxOption("featureTypes", "Show InterPro Terms:", values, values.get(1));
        featureType.setDescription("Choose how the InterPro features will be displayed");

    }

    private void appTypes() {
        beginAlignHorizontally("Applications To Run:", true);
        prodom = addBooleanOption("ProDom", "ProDom", true);
        prodom.setAdvanced(true);
        
        prints = addBooleanOption("PRINTS", "PRINTS", true);
        prints.setAdvanced(true);
        
        pirsf = addBooleanOption("PIRSF", "PIRSF", true);
        pirsf.setAdvanced(true);
        
        pfama = addBooleanOption("PfamA", "PfamA", true);
        pfama.setAdvanced(true);
        endAlignHorizontally();

        beginAlignHorizontally("", true);
        smart = addBooleanOption("SMART", "SMART", true);
        smart.setAdvanced(true);
        
        tigrfam = addBooleanOption("TIGRFAM", "TIGRFAM", true);
        tigrfam.setAdvanced(true);
        
        prositeProfiles = addBooleanOption("PrositeProfiles", "PrositeProfiles", true);
        prositeProfiles.setAdvanced(true);
        
        hamap = addBooleanOption("HAMAP", "HAMAP", true);
        hamap.setAdvanced(true);
        endAlignHorizontally();

        beginAlignHorizontally("", true);
        prositePatterns = addBooleanOption("PrositePatterns", "PrositePatterns", true);
        prositePatterns.setAdvanced(true);
        
        superFamily = addBooleanOption("SuperFamily", "SuperFamily", true);
        superFamily.setAdvanced(true);
        
        signalp = addBooleanOption("SignalP", "SignalP", true);
        signalp.setAdvanced(true);
        
        tmhmm = addBooleanOption("TMHMM", "TMHMM", true);
        tmhmm.setAdvanced(true);
        endAlignHorizontally();

        beginAlignHorizontally("", true);
        panther = addBooleanOption("Panther", "Panther", true);
        panther.setAdvanced(true);
        
        gene3d = addBooleanOption("Gene3d", "Gene3d", true);
        gene3d.setAdvanced(true);
        
        phobius = addBooleanOption("Phobius", "Phobius", true);
        phobius.setAdvanced(true);
        
        endAlignHorizontally();
    }

    public String getEmailAddress() {
        return emailAddress.getValue();
    }

    public String getFeatureTypeOption() {
        return featureType.getValue().toString();
    }

    /**
     * @return the extraFeature
     */
    public boolean getExtraFeature() {
        return extraFeature.getValue();
    }

    @Override
    protected JPanel createPanel() {
        //String THE_URL = "http://www.michaelrthon.com/geneious-interproscan-plugin/";
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel defaultPanel = super.createPanel();
        mainPanel.add(defaultPanel, BorderLayout.CENTER);
        GEditorPane citationPane = new GEditorPane();
        citationPane.setEditable(false);
        citationPane.setOpaque(false);
        citationPane.setContentType("text/html");
        citationPane.addHyperlinkListener(new SimpleLinkListener());
        citationPane.setText("<html><br><i><center>"
                + "For more information about InterPro, please visit the <a href=\"http://www.ebi.ac.uk/interpro/\">InterPro homepage </a><br>"
                + "For help using this plugin, see the <a href=\"http://www.michaelrthon.com/geneious-interproscan-plugin/\">online manual</a>"
                + "</center></i></html>");
        mainPanel.add(citationPane, BorderLayout.SOUTH);
        return mainPanel;
    }

    @Override
    public String verifyOptionsAreValid() {
        if (getEmailAddress().equals("") ||
                !getEmailAddress().contains("@")) {
            return "Please Enter A Valid Email Address";
        } else {
            return null;
        }
    }

   public BooleanOption getProdom() {
        return prodom;
    }

    public BooleanOption getPrints() {
        return prints;
    }

    public BooleanOption getPirsf() {
        return pirsf;
    }

    public BooleanOption getPfama() {
        return pfama;
    }

    public BooleanOption getSmart() {
        return smart;
    }

    public BooleanOption getTigrfam() {
        return tigrfam;
    }

    public BooleanOption getPrositeProfiles() {
        return prositeProfiles;
    }

    public BooleanOption getHamap() {
        return hamap;
    }

    public BooleanOption getPrositePatterns() {
        return prositePatterns;
    }

    public BooleanOption getSuperFamily() {
        return superFamily;
    }

    public BooleanOption getSignalp() {
        return signalp;
    }

    public BooleanOption getTmhmm() {
        return tmhmm;
    }

    public BooleanOption getPanther() {
        return panther;
    }

    public BooleanOption getGene3d() {
        return gene3d;
    }

    public BooleanOption getPhobius() {
        return phobius;
    }
}
