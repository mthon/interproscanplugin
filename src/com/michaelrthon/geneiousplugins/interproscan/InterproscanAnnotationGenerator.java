package com.michaelrthon.geneiousplugins.interproscan;

import java.util.List;

import jebl.util.ProgressListener;

import com.biomatters.geneious.publicapi.documents.AnnotatedPluginDocument;
import com.biomatters.geneious.publicapi.documents.sequence.AminoAcidSequenceDocument;
import com.biomatters.geneious.publicapi.plugin.DocumentOperationException;
import com.biomatters.geneious.publicapi.plugin.DocumentSelectionSignature;
import com.biomatters.geneious.publicapi.plugin.GeneiousActionOptions;
import com.biomatters.geneious.publicapi.plugin.Options;
import com.biomatters.geneious.publicapi.plugin.SequenceAnnotationGenerator;

/**
 * Generic template for AnnotationGenerator
 */
class InterproscanAnnotationGenerator extends SequenceAnnotationGenerator {

    static final String HELP = "InterProScan detects functional domains in proteins";

    static List<InterproscanAnnotationGenerator> scanGens = null;

    @Override
    public GeneiousActionOptions getActionOptions() {
        return new GeneiousActionOptions("Find Protein Domains With InterProScan...").setMainMenuLocation(GeneiousActionOptions.MainMenu.AnnotateAndPredict);
    }

    @Override
    public String getHelp() {
        return HELP;
    }

    @Override
    public DocumentSelectionSignature[] getSelectionSignatures() {
        return new DocumentSelectionSignature[]{
                    new DocumentSelectionSignature(AminoAcidSequenceDocument.class, 1, Integer.MAX_VALUE)
                };
    }

    @Override
    public List<SequenceAnnotationGenerator.AnnotationGeneratorResult> generate(
            AnnotatedPluginDocument[] documents,
            SequenceAnnotationGenerator.SelectionRange selectionRange,
            ProgressListener progressListener,
            Options options)
            throws DocumentOperationException {

            Runner runner = new Runner();
            //System.out.println("starting with this many docs: " + documents.length);
        List<SequenceAnnotationGenerator.AnnotationGeneratorResult> resultList =
               runner.scanSeqs(documents, selectionRange, progressListener, options);
        //System.out.println("got this many results: " + resultList.size());
        //TODO: is it possible to call save here?
        return resultList;
    }

    @Override
    public Options getOptions(AnnotatedPluginDocument[] documents,
            SequenceAnnotationGenerator.SelectionRange selectionRange)
            throws DocumentOperationException {
        return new InterproscanOptions();
    }

    @Override
    public Options getGeneralOptions() {
        return new InterproscanOptions();
    }


}
